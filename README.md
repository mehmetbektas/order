Q1 - What is dependency injection (DI)? What are the types of DI?
A1- It is managing inversion of beans. With dependency injection, depended on beans are defined. 

Q2 - Describe the Spring bean lifecycle.
A2- It manages bean creating and destroying while spring application context running, managed by spring container.

Q3 - ​ What is Spring Security?
A3 - It is managing authentication and authorization web services. It has security filters for covering services.
