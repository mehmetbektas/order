package tr.com.market.order.service;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import tr.com.market.order.common.response.Response;
import tr.com.market.order.common.response.ResponseHelper;
import tr.com.market.order.config.Configurations;
import tr.com.market.order.repository.OrderRepository;
import tr.com.market.order.repository.entity.Order;
import tr.com.market.order.service.dto.CreateOrderDto;
import tr.com.market.order.service.dto.OrderDto;
import tr.com.market.order.service.dto.UpdateOrderDto;
import tr.com.market.order.service.mapper.OrderMapper;

@Service
@RequiredArgsConstructor
public class OrderService {

	private final Configurations configurations;
	private final OrderRepository orderRepository;
	
	
	public Response find(Long id) {
		Optional<Order> order = orderRepository.findById(id);
		
		if(order.isEmpty()) {
			return ResponseHelper.error();
		}
		
		OrderDto orderDto = OrderMapper.INSTANCE.convertOrderToOrderDto(order.get());
		
		return ResponseHelper.ok(orderDto);
	}

	public Response save(CreateOrderDto createOrderDto) {
		if(!createOrderDto.getOrderDate().isAfter(LocalDate.now())) {
			return ResponseHelper.error();
		}
		
		//TODO maxOrderCount check is missing.
		
		Order order = OrderMapper.INSTANCE.convertCreateOrderDtoToOrder(createOrderDto);
		orderRepository.save(order);
		OrderDto orderDto = OrderMapper.INSTANCE.convertOrderToOrderDto(order);
		return ResponseHelper.ok(orderDto);
	}

	public Response update(Long id, UpdateOrderDto updateOrderDto) {
		if(!updateOrderDto.getOrderDate().isAfter(LocalDate.now())) {
			return ResponseHelper.error();
		}
		
		//TODO maxOrderCount check is missing.
	
		Optional<Order> orderOptional = orderRepository.findById(id);
		if(orderOptional.isEmpty()) {
			return ResponseHelper.error();
		}
				
		Order order = orderOptional.get();
		order = OrderMapper.INSTANCE.convertUpdateOrderDtoToOrder(updateOrderDto, order);
		orderRepository.save(order);
		OrderDto orderDto = OrderMapper.INSTANCE.convertOrderToOrderDto(order);
		return ResponseHelper.ok(orderDto);
	}

	public Response delete(Long id) {
		orderRepository.deleteById(id);
		return ResponseHelper.ok();
	}

}
