package tr.com.market.order.service.dto;

import java.time.LocalDate;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class OrderDto {
	
	private Long orderId;
	private Long customerId;
	private Long productId;
	private LocalDate orderDate;
}
