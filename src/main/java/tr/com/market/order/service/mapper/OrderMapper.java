package tr.com.market.order.service.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

import tr.com.market.order.repository.entity.Order;
import tr.com.market.order.service.dto.CreateOrderDto;
import tr.com.market.order.service.dto.OrderDto;
import tr.com.market.order.service.dto.UpdateOrderDto;

@Mapper
public interface OrderMapper {

	OrderMapper INSTANCE = Mappers.getMapper(OrderMapper.class);
	
	
	Order convertCreateOrderDtoToOrder(CreateOrderDto createOrderDto);
	
	Order convertUpdateOrderDtoToOrder(UpdateOrderDto updateOrderDto, @MappingTarget Order order);
	
	/*
	 * Database id can be ignore in mapping for data safety. UUID can use instead of id.
	 */
	OrderDto convertOrderToOrderDto(Order order);
	
	
}
