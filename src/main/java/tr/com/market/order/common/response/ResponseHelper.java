package tr.com.market.order.common.response;

public class ResponseHelper {

	public static Response ok() {
		return new Response(false, null, Message.SUCCESS);
	}

	public static Response ok(Object data) {
		return new Response(false, data, Message.SUCCESS);
	}

	public static Response error() {
		return new Response(true, null, Message.ERROR);
	}

}
