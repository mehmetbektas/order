package tr.com.market.order.common.response;

import lombok.Data;

@Data
public class Response {
	
	private boolean hasError;
	private Object data;
	private Message message;

	public Response(boolean hasError, Message message) {
		this.hasError = hasError;
		this.message = message;
	}

	public Response(boolean hasError, Object data, Message message) {
		this.hasError = hasError;
		this.data = data;
		this.message = message;
	}

	
	
}
