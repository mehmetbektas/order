package tr.com.market.order.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;

@Configuration
@ConfigurationProperties(prefix = "configurations")
@EnableConfigurationProperties
@Getter
public class Configurations {
	private int minOrderCount;
	
}
