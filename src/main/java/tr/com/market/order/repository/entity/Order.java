package tr.com.market.order.repository.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity(name = "order")
public class Order {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(name = "fk_customer_id")
	private Long customerId;
	@Column(name = "fk_product_id")
	private Long productId;
	@Column(name = "order_date")
	private LocalDate orderDate;
	
	
}
