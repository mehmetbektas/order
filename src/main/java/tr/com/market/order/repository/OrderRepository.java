package tr.com.market.order.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import tr.com.market.order.repository.entity.Order;

@Repository
public interface OrderRepository extends CrudRepository<Order, Long> {

}
