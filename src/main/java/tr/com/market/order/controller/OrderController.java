package tr.com.market.order.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import tr.com.market.order.common.response.Response;
import tr.com.market.order.service.OrderService;
import tr.com.market.order.service.dto.CreateOrderDto;
import tr.com.market.order.service.dto.UpdateOrderDto;

@RestController
@RequestMapping("/order")
@RequiredArgsConstructor
public class OrderController {
	
	private final OrderService orderService;
	
	@GetMapping("/{id}")
	public ResponseEntity<Response> find(@PathVariable("id") Long id) {
		Response response = orderService.find(id);
		return ResponseEntity.ok(response);
	}
	
	@PostMapping
	public ResponseEntity<Response> save(@Validated @RequestBody CreateOrderDto createOrderDto) {
		Response response = orderService.save(createOrderDto);
		return ResponseEntity.ok(response);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Response> update(@PathVariable Long id, @Validated @RequestBody UpdateOrderDto updateOrderDto) {
		Response response = orderService.update(id, updateOrderDto);
		return ResponseEntity.ok(response);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Response> delete(@PathVariable("id") Long id) {
		Response response = orderService.delete(id);
		return ResponseEntity.ok(response);
	}
	
}
