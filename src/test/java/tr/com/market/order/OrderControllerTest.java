package tr.com.market.order;

import java.time.LocalDate;
import java.util.Optional;

import javax.persistence.EntityManagerFactory;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import tr.com.market.order.common.response.Response;
import tr.com.market.order.controller.OrderController;
import tr.com.market.order.repository.OrderRepository;
import tr.com.market.order.repository.entity.Order;
import tr.com.market.order.service.dto.CreateOrderDto;
import tr.com.market.order.service.dto.OrderDto;
import tr.com.market.order.service.dto.UpdateOrderDto;

@SpringBootTest
@MockBeans(@MockBean(EntityManagerFactory.class))
class OrderControllerTest {

	@Autowired
	OrderController orderController; 
	@MockBean
	OrderRepository orderRepository;
	
	@Test
	void findTest() {
		Order order = new Order();
		order.setId(1l);
		order.setCustomerId(2l);
		order.setProductId(3l);
		order.setOrderDate(LocalDate.of(2021, 12, 21));
		
		Mockito.when(orderRepository.findById(Mockito.eq(1l))).thenReturn(Optional.of(order));
		
		ResponseEntity<Response> responseEntity = orderController.find(1l);
		
		Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	 	OrderDto orderDto = (OrderDto) responseEntity.getBody().getData();
	 	Assertions.assertEquals(order.getId(), orderDto.getOrderId());
	 	Assertions.assertEquals(order.getCustomerId(), orderDto.getCustomerId());
	 	Assertions.assertEquals(order.getProductId(), orderDto.getProductId());
	 	Assertions.assertEquals(order.getOrderDate(), orderDto.getOrderDate());
	 	
	 	
	}
	
	@Test
	void saveTest() {

		CreateOrderDto createOrderDto = new CreateOrderDto();
		createOrderDto.setCustomerId(1l);
		createOrderDto.setProductId(2l);
		createOrderDto.setOrderDate(LocalDate.of(2021, 12, 21));
		
		
		Order order = new Order();
		order.setId(1l);
		order.setProductId(createOrderDto.getProductId());
		order.setCustomerId(createOrderDto.getCustomerId());
		order.setOrderDate(createOrderDto.getOrderDate());
		
		Mockito.when(orderRepository.save(Mockito.any())).thenReturn(Optional.of(order));
		
		ResponseEntity<Response> responseEntity = orderController.save(createOrderDto);
		
		Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		OrderDto orderDto = (OrderDto) responseEntity.getBody().getData();
	 	Assertions.assertEquals(createOrderDto.getCustomerId(), orderDto.getCustomerId());
	 	Assertions.assertEquals(createOrderDto.getProductId(), orderDto.getProductId());
	 	
		
	}
	
	@Test
	void updateTest() {

		UpdateOrderDto updateOrderDto = new UpdateOrderDto();
		updateOrderDto.setCustomerId(1l);
		updateOrderDto.setProductId(2l);
		updateOrderDto.setOrderDate(LocalDate.of(2021, 12, 21));
		
		
		Order order = new Order();
		order.setId(1l);
		order.setProductId(updateOrderDto.getProductId());
		order.setCustomerId(updateOrderDto.getCustomerId());
		order.setOrderDate(updateOrderDto.getOrderDate());
		
		Mockito.when(orderRepository.save(Mockito.any())).thenReturn(Optional.of(order));
		
		ResponseEntity<Response> responseEntity = orderController.update(order.getId(),updateOrderDto);
		
		Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		OrderDto orderDto = (OrderDto) responseEntity.getBody().getData();
	 	Assertions.assertEquals(updateOrderDto.getCustomerId(), orderDto.getCustomerId());
	 	Assertions.assertEquals(updateOrderDto.getProductId(), orderDto.getProductId());
	 	
				
	}
	
	@Test
	void deleteTest() {
		ArgumentCaptor<Long> captor = ArgumentCaptor.forClass(Long.class);
		orderController.delete(1l);
		Mockito.verify(orderRepository).deleteById(captor.capture());
		Long id = captor.getValue();
		Assertions.assertEquals(1l, id);
		
	}

}
